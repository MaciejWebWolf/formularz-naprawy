<?php require_once('requiredAjaxCallCode.php'); ?>
<div class="ms-form__options-container ms-form__options-container--step-5">
<?php $data = json_decode(file_get_contents('php://input'), true);
        $id = $data["step1"]["id"]; 
        ?>


    <?php function sendParcel() { //NAPRAWA WYSYŁKOWA
        $data = json_decode(file_get_contents('php://input'), true); 

        $id = $data["step1"]["id"]; 
        $service_id = $data["step4"]["id"]; 
        $price_original = $data["step4"]["price_original"]; 
        $price_substitute = $data["step4"]["price_substitute"]; 

        $dateMin = new DateTime('tomorrow');
        $dateMin = $dateMin->format('Y-m-d');
        
        $todayDate = new DateTime('today');
        $todayDay = $todayDate->format('d');
        $todayYear = $todayDate->format('Y');
        $todayMonth = $todayDate->format('m');
        
        function getMaxDate($todayYear, $todayMonth, $todayDay ) {
            $numOfDays = date("t");
            if($todayDay + 4 > $numOfDays) {
            $day = $todayDay + 4 - $numOfDays;
            $month = $todayMonth+1;
            $dateMax = $todayYear . '-' . $month . '-0' . $day;
            return $dateMax;
            } else {
            return $dateMax = $todayYear . '-' . $todayMonth . '-' . ($todayDay + 4);
            }
          }
          
          $dateMax = getMaxDate($todayYear, $todayMonth, $todayDay );

        
        ?>
        <div class="last-step-summary">
            <h3 class="last-step-summary__heading">Podsumowanie</h3>
            <div class="last-step-summary__data">
                <div class="last-step-summary__row">
                    Sposób wykonania usługi: <span class="last-step-summary__result"><?php  echo $data["step1"]["name"] ?></span>
                </div>
                <div class="last-step-summary__row">
                    Usługa: <span class="last-step-summary__result"><?php  echo $data["step4"]["name"] ?></span>
                </div>
                <div class="last-step-summary__row">
                    Sprzęt: <span class="last-step-summary__result"><?php  echo $data["step2"]["name"] ?></span>
                </div>
                <div class="last-step-summary__row last-step-summary__row--price">
                    Koszt usługi: </br>
                    <span class="last-step-summary__result">Oryginał: <?php echo $price_original; ?></span>
                    <span class="last-step-summary__result">Zamiennik: <?php echo $price_substitute; ?></span>
                </div>
                <div class="last-step-summary__row">
                    Model: <span class="last-step-summary__result"><?php  echo $data["step3"]["name"] ?></span><span
                        class="last-step-summary__image"><img src="<?php  echo $data["step3"]["modelImageUrl"] ?>" alt=""></span>
                </div>
            </div>
        </div>
        <div class="last-step-form">
            <form  id="last-step-form__form" >
                <h3 class="last-step-form__heading">Dane osobowe</h3>
                <div class="last-step-form__inputs">
                    <div class="last-step-form__input">
                        <label for="first_name">Imię *</label>
                        <input type="text" placeholder="Imię" name="first_name" required value="Maciek">
                    </div>
                    <div class="last-step-form__input">
                        <label for="last_name">Nazwisko *</label>
                        <input type="text" placeholder="Nazwisko" name="last_name" required value="baczynski">
                    </div>
                    <div class="last-step-form__input">
                        <label for="company-name">Nazwa firmy</label>
                        <input type="text" placeholder="Nazwa firmy" name="company-name">
                    </div>
                   
                </div>
                <h3 class="last-step-form__heading">Dane kontaktowe</h3>
                <div class="last-step-form__inputs">
                    <div class="last-step-form__input">
                        <label for="email">Adres e-mail *</label>
                        <input type="email" placeholder="Adres e-mail" name="email" required value="baciey@gmail.com">
                    </div>
                    <div class="last-step-form__input">
                        <label for="phone">Numer telefonu *</label>
                        <input type="tel" placeholder="Numer telefonu" name="phone" required value="222333444">
                    </div>
                    <div class="last-step-form__input last-step-form__input--message">
                        <label for="message">Wiadomość</label>
                        <textarea name="message" cols="30" rows="7" style="resize:none"></textarea>
                    </div>
                </div>
                <h3 class="last-step-form__heading">Adres odbioru sprzętu</h3>
                <div class="last-step-form__inputs">
                    <div class="last-step-form__input">
                        <label for="postal_code">Kod pocztowy *</label>
                        <input type="text" placeholder="Kod pocztowy" name="postal_code" required value="52-437">
                    </div>
                    <div class="last-step-form__input">
                        <label for="city">Miasto *</label>
                        <input type="text" placeholder="Miasto" name="city" required value="Wrocław">
                    </div>
                    <div class="last-step-form__input">
                        <label for="street">Ulica *</label>
                        <input type="text" placeholder="Ulica" name="street" required value="Balzaka">
                    </div>
                    <div class="last-step-form__input">
                        <label for="house_number">Numer domu/lokalu *</label>
                        <input type="text" placeholder="Numer domu/lokalu" name="house_number" required value="32/6">
                    </div>
                    <div class="last-step-form__input">
                        <label for="date">Data *
                         </label>
                            <div class="question-circle">
                                <img src="<?php echo WW_URL . "/icons/question-circle.svg";  ?>" alt="">
                                <p class="ms-form-info-box">
                                Kuriera można zamówić na dzień roboczy, maksymalnie z 3 dniowym wyprzedzeniem.
                                <span class="ms-form-info-box__triangle-bottom"></span>
                                </p>
                            </div>
                        
                        <input type="date" name="date" value="<?php echo $dateMin;?>" id="pick-up-date" min="<?php echo $dateMin;?>" max="<?php echo $dateMax;?>" required>
                    </div>
                    <div class="checkbox-area">
                        <p>Wymagane *</p>
                        <div class="checkbox-area__row">
                            <input type="checkbox" name="condition1" required checked="true">
                            <label for="condition1">Oświadczam, iż zapoznałem(am) się z oficjalną informacją producenta
                                dot. wodoszczeloności: iPhone: <span class="bold">
                                    https://support.apple.com/pl-pl/HT20704</span>, Apple Watch:
                                <span class="bold">https://support.apple.com/pl-pl/HT205000</span> *</label>
                        </div>
                        <div class="checkbox-area__row">
                            <input type="checkbox" name="condition2" required checked="true">
                            <label for="condition2">Oświadczam, że rozumiem, iż każde uszkodzenie mechanicze, ingerencja
                                serwisowa, powoduje utratę wodoszczelności mojego urządzenia. *</label>
                        </div>
                        <div class="checkbox-area__row">
                            <input type="checkbox" name="condition3" required checked="true">
                            <label for="condition3">Oświadczam, że rozumiem, iż w przypadku nieodebrania urządzenia po
                                upływie 3 miesięcy od zakończenia naprawy, urządzenia zostanie zutylizowane. *</label>
                        </div>
                        <div class="checkbox-area__row">
                            <input type="checkbox" name="condition4" required checked="true">
                            <label for="condition4">Administratorem danych osobowych jest JG GRUPA Jakub Groszek z siedzibą
                                pod adresem: Plac Bankowy 2 00-095 Warszawa, Polska . Dane wpisane w formularzu kontaktowym
                                będą przetwarzane w celu udzielenia odpowiedzi na przesłane zapytanie zgodnie z <a
                                    class="bold" href="/polityka-prywatnosci"> polityką
                                    prywatności</a>. * </label>
                        </div>
                        <div class="checkbox-area__row">
                            <input type="checkbox" name="condition5" required checked="true">
                            <label for="condition5">Podpisując dokument oświadczam, że zapoznałem(am) się z regulaminem
                                serwisu. *</label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
    <?php }
    

    function visitUs() { //Odwiedź nas w salonie 
    
        $data = json_decode(file_get_contents('php://input'), true); ?>
        

        <div class="last-step-summary">
            <h3 class="last-step-summary__heading">Podsumowanie</h3>
            <div class="last-step-summary__data">
                <div class="last-step-summary__row">
                    Sposób wykonania usługi: 
                    <span class="last-step-summary__result"><?php  echo $data["step1"]["name"] ?></span>
                </div>
            </div>
        </div>
        <div class="last-step-form">
            <form  id="last-step-form__form" >
                <h3 class="last-step-form__heading">Wybierz salon / miasto</h3>
                <div class="last-step-form__inputs">
                    <div class="last-step-form__input">
                        <label for="saloon">Salon</label>
                        <select name="saloon" id="last-step-form__select" onchange="step.handleSelectChange()">
                            <option value="wola">Warszawa wola/śródmieście</option>
                            <option value="mokotow">Warszawa mokotów</option>
                            <option value="krakow">Kraków</option>
                            <option value="katowice">Katowice</option>
                        </select>
                    </div>

                    <div class="last-step-form__input">
                        <div class="select-result">
                            <div class="select-result__row">
                                <span class="select-result__icon marker"></span>
                                <p class="select-result__location"> ul. Dzielna 64 (Wejście od Bellottiego) Parking przed lokalem</p>
                            </div>
                            <div class="select-result__row bold">
                                <span class="select-result__icon phone"></span>
                                <a class="flixapple-display-number" href="#">pokaż numer telefonu</a>
                            </div>
                            <div class="select-result__row bold">
                                <span class="select-result__icon email"></span>
                                <a class="flixapple-display-email" href="#">POKAŻ ADRES E-MAIL</a>
                            </div>
                            <div class="select-result__row info">
                                <p> Serwis stacjonarny czynny:</p>
                                <p class="select-result__opening-1"> Poniedziałek – Piątek 10:00 – 19:00</p>
                                <p class="select-result__opening-2"> Sobota 10:00 – 17:00 </p>
                                </br>
                                <p> Infolinia czynna 7 dni w tygodniu </p>
                                <p> 9:00 – 19:00</p>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox-area">
                        <p>Wymagane *</p>
                        <div class="checkbox-area__row">

                            <input type="checkbox" name="condition4" required>
                            <label for="condition4">Administratorem danych osobowych jest JG GRUPA Jakub Groszek z siedzibą
                                pod adresem: Plac Bankowy 2 00-095 Warszawa, Polska . Dane wpisane w formularzu kontaktowym
                                będą przetwarzane w celu udzielenia odpowiedzi na przesłane zapytanie zgodnie z <a
                                    class="bold" href="/polityka-prywatnosci"> polityką
                                    prywatności</a>. * </label>
                        </div>
                    </div>
                </div> 
            </form>
        </div>
    <?php }
    if ($id == 2) visitUs();
    else if ($id == 3) sendParcel(); ?>

    
    
    
</div>
   

