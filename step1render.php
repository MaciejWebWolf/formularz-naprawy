<?php require_once('requiredAjaxCallCode.php'); ?>

<?php

 define('WW_URL', plugin_dir_url(__FILE__));


?>

<div class="ms-form__options-container ms-form__options-container--step-1">
    <div class="ms-form__option--step-1 ms-form__option" id="1" data-name="Zarezerwuj termin usługi">
        <div class="question-circle">
            <img src="<?php echo WW_URL . "/icons/question-circle.svg";  ?>" alt="">
            <p class="ms-form-info-box">
            Brak wolnego terminu nie wiąże się z brakiem obslugi. Dołożymy wszelkich starań, by usługa została wykonana możliwie jak najszybciej
            <span class="ms-form-info-box__triangle-bottom"></span>
            </p>
        </div>
        <div class="ms-form__col-icon"><img src="<?php echo WW_URL . "/icons/calendar.svg";  ?>" alt=""></div>
        <div class="ms-form__col-title">Zarezerwuj termin usługi</div>
        <div class="ms-form__col-desc">Nie chcesz stać w kolejce?  Zarezerwuj termin naprawy</div>
    </div>
    <div class="ms-form__option--step-1 ms-form__option " id="2" data-name="Odwiedź nas w salonie">
        <div class="ms-form__col-icon"><img src="<?php echo WW_URL . "/icons/map-marker-alt.svg";  ?>" alt=""></div>
        <div class="ms-form__col-title">Odwiedź nas w salonie</div>
        <div class="ms-form__col-desc">Nie wiesz o której godzinie przyjedziesz do naszego salonu? Przyjdź do nas bez rezerwacji. </br></br> Zrobimy wszystko, by usługa została wykonana ekspresowo!</div>
    </div>
    <div class="ms-form__option--step-1 ms-form__option " id="3" data-name="Naprawa wysyłkowa">
        <div class="ms-form__col-icon"><img src="<?php echo WW_URL . "/icons/shipping-fast.svg";  ?>" alt=""></div>
        <div class="ms-form__col-title">Naprawa wysyłkowa</div>
        <div class="ms-form__col-desc">Nie chcesz wychodzić z domu? Zamów darmowego kuriera!</div>
    </div>
</div>
