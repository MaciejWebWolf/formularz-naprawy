class Steps {
  constructor() {
    this.msFormData = {
      step1: {
        id: 3,
        name: "Naprawa wysyłkowa",
        backButtonText: "Cofnij",
        nextButtonText: "Wybierz sprzęt",
        nextButtonHoverText: "Wybierz usługę by móc przejść dalej",
      },
      step2: {
        id: null,
        name: null,
        backButtonText: "Wybierz sposób wykonania usługi",
        nextButtonText: "Wybierz model",
        nextButtonHoverText: "Wybierz sprzęt by móc przejść dalej",
      },
      step3: {
        id: null,
        name: null,
        backButtonText: "Wybierz sprzęt",
        nextButtonText: "Wybierz usługę",
        nextButtonHoverText: "Wybierz model by móc przejść dalej",
        modelImageUrl: null,
      },
      step4: {
        id: null,
        name: null,
        backButtonText: "Wybierz model",
        nextButtonText: "Podsumowanie",
        nextButtonHoverText: "Wybierz typ naprawy by móc przejść dalej",
      },
      step5: {
        id: null,
        name: null,
        backButtonText: "Wybierz usługę",
        nextButtonText: "Wyślij zgłoszenie",
        nextButtonHoverText:
          "Wypełnij wszystkie wymagane pola by wysłać zgłoszenie",
      },
      action: "save-ajax-request", // action needed to save whole form data
      // saloon: "",
    };
    this.msFormDataa = {
      step1: {
        id: 3,
        name: "naprawa wysylkowa",
        errorMsg: "Proszę wybrać typ usługi",
        backButtonText: "Cofnij",
        nextButtonText: "Wybierz sprzęt",
      },
      step2: {
        id: 23,
        name: "Apple Watch",
        errorMsg: "Proszę wybrać kategorię produktu",
        backButtonText: "Wybierz sposób wykonania usługi",
        nextButtonText: "Wybierz model",
      },
      step3: {
        id: 674,
        name: "Apple Watch 7",
        errorMsg: "Proszę wybrać model",
        backButtonText: "Wybierz sprzęt",
        nextButtonText: "Wybierz usługę",
        modelImageUrl: null,
      },
      step4: {
        id: 386,
        name: "wymiana szybki",
        errorMsg: "Proszę wybrać typ naprawy",
        backButtonText: "Wybierz model",
        nextButtonText: "Podsumowanie",
        price_original: 120,
        price_substitute: 80,
      },
      step5: {
        id: null,
        name: null,
        errorMsg: "",
        backButtonText: "Wybierz usługę",
        nextButtonText: "Wyślij zgłoszenie",
      },

      action: "save-ajax-request", // action needed to save whole form data
      // saloon: "wola",
    };
    this.stepNumber = 1;
    this.isFormOkay = false;

    this.clickBlock = false;
    this.inputsValue = {};
    this.buttonsContainer = document.querySelector(
      ".ms-form__buttons-container"
    );
    this.backButton = this.buttonsContainer.querySelector(
      ".ms-form__button--back"
    );
    this.nextButton = this.buttonsContainer.querySelector(
      ".ms-form__button--next"
    );
    this.areInputsOkay = {
      first_name: false,
      last_name: false,
      date: false,
      email: false,
      phone: false,
      postal_code: false,
      city: false,
      street: false,
      house_number: false,
      checkbox: {
        condition1: false,
        condition2: false,
        condition3: false,
        condition4: false,
        condition5: false,
      },
    };
    this.areInputsOkayy = {
      first_name: true,
      last_name: true,
      date: true,
      email: true,
      phone: true,
      postal_code: true,
      city: true,
      street: true,
      house_number: true,
      checkbox: {
        condition1: true,
        condition2: true,
        condition3: true,
        condition4: true,
        condition5: false,
      },
    };
    this.areInputsOkayVisitUs = {
      saloon: "wola",
      checkbox: {
        condition4: false,
      },
    };
  }

  switchStep(num, e) {
    if (this.clickBlock === false) {
      if (this.stepNumber === 1 && num === -1)
        return console.log("blokada ponizej step 1");

      //STEP 5 'WYŚLIJ ZGŁOSZENIE IS CLICKED'
      if (this.stepNumber === 5 && num === 1) {
        if (this.isFormOkay) return this.sendFormData(e);
      }

      if (this.msFormData["step" + this.stepNumber].id === null && num === 1) {
        null;
      } else {
        //JUMP TO STEP 5 IF 'ODWIEDZ NAS W SALONIE' HAS BEEN CHOSEN [STEP 1]
        if (this.msFormData.step1.id == 2 && this.stepNumber == 1)
          this.stepNumber = 5;
        //JUMP TO STEP 1 IF 'ODWIEDZ NAS W SALONIE' HAS BEEN CHOSEN [STEP 5]
        else if (this.msFormData.step1.id == 2 && num == -1) {
          this.stepNumber = 1;
        } else this.stepNumber += num;

        this.clickBlock = true;
        this.handleAjax();
      }
    }
  }
  renderHtml(response) {
    const container = document.querySelector(".ms-form__options-container");
    if (this.stepNumber < 6) {
      container.innerHTML = response;
      this.switchNextButton();

      //HIDE/SHOW BUTTON ON STEP 1
      if (this.stepNumber > 1) this.backButton.classList.add("visible");
      else this.backButton.classList.remove("visible");

      const currentStep = this.msFormData["step" + this.stepNumber];

      //DISPLAY PROPER BUTTONS TEXT
      this.nextButton.childNodes[1].childNodes[0].textContent =
        currentStep.nextButtonHoverText;
      this.nextButton.childNodes[0].textContent = currentStep.nextButtonText;
      this.backButton.textContent = currentStep.backButtonText;

      //CHANGE BACK BUTTON TEXT IF 'ODWIEDZ NAS W SALONIE' HAS BEEN CHOSEN
      if (this.msFormData.step1.id == 2) {
        if (this.stepNumber == 5) {
          this.backButton.childNodes[0].textContent =
            "Wybierz sposób wykonania usługi";
        }
      }

      //STEP BAR
      const renderStepBar = () => {
        const step1Id = this.msFormData.step1.id;
        const step5 = document.querySelector(".step-bar__step--5");
        const steps = document.querySelectorAll(".step-bar__step");
        const activeStep = document.querySelector(
          `.step-bar__step--${this.stepNumber}`
        );

        if (step1Id == 2) {
          //ODWIEDŹ NAS W SALONIE (ONLY STEP 1 & 5 CAN BE ACTIVE)
          if (this.stepNumber == 5)
            step5.classList.add("active", "semi-active");
          else step5.classList.remove("active", "semi-active");
        } else if (step1Id == 3) {
          //NAPRAWA WYSYŁKOWA
          steps.forEach((step) => {
            step.classList.remove("active");
          });
          for (let i = this.stepNumber; i > 0; i--) {
            const activeStep = document.querySelector(`.step-bar__step--${i}`);
            if (activeStep) activeStep.classList.add("active");
          }
        }

        //DISPLAY ONLY CURRENT STEP IF SCREEN WIDTH IS LESS THAN 481PX
        if (window.innerWidth < 481) {
          steps.forEach((step) => {
            step.classList.remove("visible");
          });
          activeStep.classList.add("visible");
        } else {
          activeStep.classList.remove("visible");
        }
      };
      renderStepBar();
    }

    //STEP 6
    else if (this.stepNumber === 6) {
      const container = document.querySelector(".ms-form__container-outter");
      container.innerHTML = response;
    }
  }

  addEvents() {
    const options = document.querySelectorAll(
      `.ms-form__option--step-${this.stepNumber}`
    );
    if (options)
      options.forEach((option) => {
        option.addEventListener("click", function () {
          step.chooseOption(this);
        });
      });
    if (this.stepNumber === 5) {
      //DATE PICKER
      const picker = document.getElementById("pick-up-date");
      if (picker) picker.addEventListener("input", pickUpDate);

      //FORM
      const form = document.querySelector(".last-step-form");
      const inputs = form.querySelectorAll("input");
      const requiredInputs = form.querySelectorAll("[required]");

      //REMEMBER INPUTS VALUES
      inputs.forEach((input) => {
        input.addEventListener("input", function () {
          step.rememberInputValue(this);
        });
        const value = this.inputsValue[input.name];
        if (input.name in this.inputsValue && input.type == "checkbox")
          return (input.checked = this.inputsValue[input.name]);
        if (value) input.value = this.inputsValue[input.name];
      });

      //VALIDATE REQUIRED INPUTS
      requiredInputs.forEach((input) => {
        input.addEventListener("input", this.validateSingleInput);
      });

      showNumberAndEmail();
    }
  }

  addClassList() {
    const id = this.msFormData["step" + this.stepNumber].id;
    if (id) {
      const el = document.getElementById(id);
      if (el) el.classList.add("active");

      this.switchNextButton();
    }
  }
  switchNextButton() {
    //DISABLE OR ENABLE NEXT BUTTON
    const disableButton = () => {
      this.nextButton.disabled = true;
      this.nextButton.classList.add("disabled");
    };
    const enableButton = () => {
      this.nextButton.disabled = false;
      this.nextButton.classList.remove("disabled");
    };

    //CHANGE NEXT BUTTON TEXT IF 'ODWIEDZ NAS W SALONIE' HAS BEEN CHOSEN
    const activeEl = document.querySelector(".ms-form__option.active");
    if (activeEl) {
      if (this.stepNumber == 1) {
        if (activeEl.id == 2) {
          this.nextButton.childNodes[0].textContent = "Przejdź dalej";
        } else if (activeEl.id == 3) {
          this.nextButton.childNodes[0].textContent = "Wybierz sprzęt";
        }
      }
    }

    if (this.stepNumber < 5) {
      const options = document.querySelectorAll(".ms-form__option");
      disableButton();
      if (options) {
        options.forEach((option) => {
          if (option.classList.contains("active")) {
            enableButton();
          }
        });
      }
    } else this.isFormOkay ? enableButton() : disableButton();
  }
  rememberInputValue(input) {
    if (input.type == "checkbox")
      return (this.inputsValue[input.name] = input.checked);
    this.inputsValue[input.name] = input.value;
  }

  handleAjax() {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = () => {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        this.renderHtml(xmlhttp.responseText);
        this.addEvents();
        this.addClassList();
        this.clickBlock = false;
      }
    };
    const json = JSON.stringify(this.msFormData);
    xmlhttp.open("POST", `${url.plugin_path}/step${this.stepNumber}render.php`);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(json);
  }
  chooseOption(element) {
    const currentStep = this.msFormData["step" + this.stepNumber];
    const price_subs = element.getAttribute("data-price_substitute");
    const price_ori = element.getAttribute("data-price_original");
    const name = element.getAttribute("data-name");
    const options = document.querySelectorAll(".ms-form__option");

    options.forEach((option) => option.classList.remove("active"));
    element.classList.add("active");
    this.switchNextButton();
    currentStep.id = element.id;
    currentStep.name = name;

    if (this.stepNumber === 3) {
      const imageUrl = element.childNodes[1].childNodes[0].currentSrc;
      currentStep.modelImageUrl = imageUrl;
    }
    if (this.stepNumber === 4) {
      currentStep.price_original = price_ori;
      currentStep.price_substitute = price_subs;
    }
  }
  sendFormData(e) {
    const form = jQuery("#last-step-form__form");
    e.preventDefault();
    const urls = `${url.plugin_path}step6render.php`;
    jQuery.ajax({
      type: "POST",
      url: urls,
      data: { deviceData: this.msFormData, formData: form.serialize() },
      success: function (json) {
        console.log(json);
        const data = JSON.parse(json);
        if (data.status == "error") {
          alert(data.data);
        } else {
          step.stepNumber += 1;
          step.renderHtml(data.data);
        }
      },
    });
  }
  handleSelectChange() {
    const value = document.getElementById("last-step-form__select").value;
    const location = document.querySelector(".select-result__location");
    const opening1 = document.querySelector(".select-result__opening-1");
    const opening2 = document.querySelector(".select-result__opening-2");

    location.textContent = saloonsData[value].location;
    opening1.textContent = saloonsData[value].opening[0];
    opening2.textContent = saloonsData[value].opening[1];

    this.areInputsOkayVisitUs.saloon = value;
  }

  validateSingleInput() {
    const formCheck = () => {
      if (step.msFormData.step1.id == 2)
        step.formCheck(step.areInputsOkayVisitUs);
      else if (step.msFormData.step1.id == 3)
        step.formCheck(step.areInputsOkay);
    };
    if (this.type !== "checkbox") {
      function inputStatus(bool, el) {
        bool
          ? el.parentNode.classList.remove("error")
          : el.parentNode.classList.add("error");
        step.areInputsOkay[el.name] = bool;
        formCheck();
      }
      //POSTAL CODE
      if (this.name == "postal_code") {
        if (this.value.length !== 6 || /[a-z]/i.test(this.value))
          inputStatus(false, this);
        else inputStatus(true, this);
      }
      //EMAIL
      else if (this.name == "email") {
        if (!this.value.includes("@")) inputStatus(false, this);
        else inputStatus(true, this);
      }
      //PHONE
      else if (this.name == "phone") {
        if (/[a-z]/i.test(this.value) || this.value.length < 1)
          inputStatus(false, this);
        else inputStatus(true, this);
      }
      //OTHER INPUTS
      else {
        if (this.value.length < 1) inputStatus(false, this);
        else inputStatus(true, this);
      }
    } else if (this.type == "checkbox") {
      if (step.msFormData.step1.id == 3) {
        step.areInputsOkay.checkbox[this.name] = this.checked;
      } else if (step.msFormData.step1.id == 2) {
        step.areInputsOkayVisitUs.checkbox[this.name] = this.checked;
      }

      formCheck();
    }
  }
  formCheck(formData) {
    const bool =
      Object.values(formData).every(Boolean) &&
      Object.values(formData.checkbox).every(Boolean);

    this.isFormOkay = bool;
    this.switchNextButton();
  }
}

const step = new Steps();
const buttonsContainer = document.querySelector(".ms-form__buttons-container");
if (buttonsContainer) {
  const backButton = buttonsContainer.querySelector(".ms-form__button--back");
  const nextButton = buttonsContainer.querySelector(".ms-form__button--next");
  if (backButton)
    backButton.addEventListener("click", (e) => step.switchStep(-1, e));
  if (nextButton) {
    nextButton.addEventListener("click", (e) => step.switchStep(1, e));
  }
}

const saloonsData = {
  wola: {
    location: "ul. Dzielna 64 (Wejście od Bellottiego) Parking przed lokalem",
    phone: "tel wola",
    email: "@1",
    opening: ["Poniedziałek – Piątek 10:00 – 19:00", "Sobota 10:00 – 17:00"],
  },
  mokotow: {
    location: "ul. Rakowiecka 45 Parking przed lokalem",
    phone: "tel mkotow",
    email: "@2",
    opening: ["Poniedziałek – Piątek 10:00 – 19:00", "Sobota 10:00 – 17:00"],
  },
  krakow: {
    location: "ul. Stefana Batorego 4A Parking przed lokalem",
    phone: "tel kra",
    email: "@3",
    opening: ["Poniedziałek – Piątek 10:00 – 18:00", "Sobota 10:00 – 17:00"],
  },
  katowice: {
    location: "ul. Wojewódzka 15",
    phone: "tel kat",
    email: "@4",
    opening: ["Poniedziałek – Piątek 10:00 – 18:00", ""],
  },
};

function backToHomePage() {
  window.location = "/";
}

function pickUpDate() {
  const day = new Date(this.value).getUTCDay();
  if ([6, 0].includes(day)) {
    this.value = "";
    alert("W ten dzień kurier nie może się zjawić, proszę wybrać inny");
    return;
  }
}

//FUNCKJA MARCELA
function showNumberAndEmail() {
  jQuery(".flixapple-display-number").on("click", function (event) {
    if (
      jQuery(this).text() == "pokaż numer telefonu" ||
      jQuery(this).text() == "Zadzwoń do nas"
    ) {
      event.preventDefault();

      jQuery(this)
        .text("577 397 907")
        .delay(500)
        .queue(function (next) {
          jQuery(this).attr("href", "tel:577-397-907");
          next();
        });
    }
  });

  jQuery(".flixapple-display-email").on("click", function (event) {
    if (
      jQuery(this).text() == "POKAŻ ADRES E-MAIL" ||
      jQuery(this).text() == "POKAŻ ADRES EMAIL"
    ) {
      event.preventDefault();

      jQuery(this)
        .text("info@flixapple.com")
        .delay(500)
        .queue(function (next) {
          jQuery(this).attr("href", "mailto:info@flixapple.com");
          next();
        });
    }
  });
}
