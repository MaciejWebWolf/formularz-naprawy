<?php require_once('requiredAjaxCallCode.php'); ?>
<?php define('WW_URL', plugin_dir_url(__FILE__)); ?>

<div class="ms-form__options-container ms-form__options-container--step-2">
    <?php function step2() {
        
        $terms = get_terms([
            'taxonomy' => "device-types",
            'hide_empty' => false,
        ]
        );
        foreach ($terms as $key => $value) {
            // var_dump($value);
             ?>
                <div class="ms-form__option--step-2 ms-form__option" data-name="<?php echo $value->name; ?>" id="<?php echo $value->term_taxonomy_id; ?>" >
                    <?php $id = 'category_' . $value->term_taxonomy_id;; ?>
                    <?php// $icon = get_field('ikona', $id);
                        // if($icon) { ?>
                        <!-- <div class="ms-form__col-icon"><img src="<?php //echo $icon['url']; ?>" alt=""></div> -->
                        <div class="ms-form__col-icon"><img src="<?php echo WW_URL . "/icons/cat/" . $value->slug . ".svg";  ?>
                " alt=""></div>

                    <!-- <?php //} ?> -->
                    <div class="ms-form__col-title"><?php echo $value->name ?></div>
                </div>
           <?php
        }
    }
    step2(); ?>
</div>
   