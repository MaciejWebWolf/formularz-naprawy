<?php require_once('requiredAjaxCallCode.php'); ?>


<?php
 define('WW_URL', plugin_dir_url(__FILE__));
//  echo WW_URL;

class DHL24_webapi_client extends SoapClient
{
    const WSDL = 'https://sandbox.dhl24.com.pl/webapi2';
    public function __construct()
    {
        parent::__construct( self::WSDL );
    }
}
$authData = [
    "username" => 'FLIXAPPLE_TEST',
    "password" => 'LOlG@Ym*7iuUr*G',
];

$formData = array();
parse_str($_POST["formData"], $formData);

$deviceData = $_POST["deviceData"];
// var_dump($deviceData["step3"]["name"]);
// var_dump($deviceData);
// var_dump($deviceData);
// var_dump($formData);






function createShipment($authData, $formData, $deviceData) {

    $postalCode = $formData["postal_code"];
    $postalCode = preg_replace('/[^0-9]/', '', $postalCode);

    $datetime = $formData["date"];

    
    $shipper = [
        "name"=> $formData["first_name"] ." ". $formData["last_name"], //required
        "postalCode"=>$postalCode,//required
        "city"=> $formData["city"],//required
        "street"=> $formData["street"],//required
        "houseNumber"=> $formData["house_number"],//required
        // "apartmentNumber" => "6",//optional
        "contactPhone" => $formData["phone"],//optional
        "contactEmail" => $formData["email"]//optional
        ];
    $receiver = [
        "addresType" => "B",//required
        "country" => "PL",//required
        "name"=>"FLIX - Serwis Apple",//required
        "postalCode"=>"01029",//required
        "city"=> "Warszawa",//required
        "street"=> "Dzielna",//required
        "houseNumber"=> "64",//required
        // "apartmentNumber" => "32",//optional
        "contactPhone"=> "577397907",//optional
        "contactEmail"=> "baciey@gmail.com",//optional

        ];
    $pieceList = [
        'type' => 'PACKAGE',//required ENVELOPE, PACKAGE, PALLET
        'width' => 10,//required
        'height' => 20,//required
        'length' => 8,//required
        'weight' => 1,//required
        'quantity' => 1,//required
        'nonStandard' => false//optional
        ];
    $payment = [
        "paymentMethod" =>"BANK_TRANSFER",//required
        "payerType"=> "SHIPPER",//required
        "accountNumber" => "6000000",//required
    ];
    $service = [
        'product' => 'AH',//required AH - przesyłka krajowa
    ];
    $shipmentDate = $datetime;//required
    $content = 'Sprzęt elektroniczny';//required zawartość przesyłki string(30)
    // $comment = $formData["message"];//optional string(100)
    $data['item'] = [
        'shipper' => $shipper,
        'receiver' => $receiver,
        'pieceList' => [
            'item' => $pieceList,
        ],
        'payment' => $payment,
        'service' => $service,
        'shipmentDate' => $shipmentDate,
        'content' => $content,
        'skipRestrictionCheck' => true //optional Czy pominąć sprawdzenie restrykcji
    ];
    $params = [
        'authData' => $authData,
        'shipments' => $data,
    ];
     

    try {
        $client = new DHL24_webapi_client;
        $result = $client->createShipments($params);
        $id = $result->createShipmentsResult->item->shipmentId;
        $shipper = $result->createShipmentsResult->item->shipper;
        $receiver = $result->createShipmentsResult->item->receiver;

        bookCourier($shipper, $receiver, $client, $authData, $id, $datetime, $deviceData, $formData);


    }
    catch (Exception $e) {
            $msg = $e->getMessage();
            $error = ["data" => $msg, "status" => "error"];
            $json = json_encode($error);
            echo $json;
    }
    
     
}


function bookCourier($shipper, $receiver, $client, $authData, $id, $datetime, $deviceData, $formData) {
    // BOOK COURIER()
    $bookCourierParams = [
        'authData' => $authData,//required
        'pickupDate' => $datetime,//required
        'pickupTimeFrom' => '10:00',//required
        'pickupTimeTo' => '20:00',//required
        'shipmentIdList' => [$id],//required
        "courierWithLabel" => true,//optional Czy kurier ma przyjechać z etykietą? Przyjazd Kuriera z listem przewozowym możliwy jest jedynie dla zleceń najwcześniej na kolejny dzień roboczy.
        // "additionalInfo" => $formData["message"], //optional Dodatkowe informacje dla kuriera string(50)
    ];

    try {
        $result2 = $client->bookCourier($bookCourierParams);
        function sendEmailToClient($shipper, $datetime, $deviceData) {
            $email_template_client = WW_URL . "email_template_client.php";
        
            // $message = file_get_contents($email_template_client);
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $to = $shipper->contactEmail; 
            $subject = "Potwierdzenie zgłoszenia naprawy";
            $message = "Kurier pojawi się dnia " . $datetime . " w godzinach 10:00 - 20:00. Adres odbioru paczki: " . $shipper->street . " " . $shipper->houseNumber . " " . $shipper->postalCode . " " . $shipper->city  . " Urządzenie do naprawy: " . $deviceData["step3"]["name"] . " Forma naprawy: " . $deviceData["step4"]["name"];
        
            $emailresult = wp_mail($to, $subject, $message, $headers);
            // var_dump($emailresult);
        }
        function sendEmailToFlix($receiver, $id, $deviceData, $formData) {
            $to = $receiver->contactEmail; 
            $subject = "Potwierdzenie zgłoszenia naprawy";
            $message = "Wiadomość od klienta: " . $formData["message"] . ". ID paczki: " . $id . " Urządzenie do naprawy: " . $deviceData["step3"]["name"] . " Forma naprawy: " . $deviceData["step4"]["name"];
        
            $emailresult = wp_mail($to, $subject, $message);
            // var_dump($emailresult);
        }

        sendEmailToClient($shipper, $datetime, $deviceData);
        sendEmailToFlix($receiver, $id, $deviceData, $formData);

        //CONFIRMATION PAGE
        ob_start(); ?>
            <div class="ms-confirmation">
                <div class="ms-confirmation__icon">
                    <img src="<?php echo WW_URL . 'icons/calendar-coloured.svg';  ?>" alt="">
                </div>
                <h3 class="ms-confirmation__heading">Twoje zgłoszenie zostało przyjęte!</h3>
                <p class="ms-confirmation__desc">W ciągu kilku minut otrzymasz na maila list przewozowy. Wydrukuj go i naklej na
                    paczkę. Pamiętaj o tym, aby zapakować urządzenie tak, żeby bezpiecznie do nas dotarło!</p>
                <button onclick="backToHomePage();" class="ms-form__button ms-form__button--next">Strona główna</button>
            </div>
        <?php $confirmationPage = ob_get_clean(); 

         $ok = ["data" => $confirmationPage, "status" => "ok"];
         $json = json_encode($ok);
         echo $json;
    }
    catch (Exception $e) {
        // echo $e->getMessage();
        var_dump("asdasdasdas");
        // echo gettype($e->getMessage());
        // $msg = strval($e->getMessage());
            $msg = $e->getMessage();
        // echo gettype($msg);

            $error = ["data" => $msg, "status" => "error"];
            $json = json_encode($error);
            echo $json;
        }
    }



function visitUsConfirmationPage($formData) { 
    
    $saloon = $formData["saloon"];
    $url = "";
    $anchor = "#section-86-46";

     if($saloon == "wola"){
        $url = get_home_url() . $anchor;
     }
     else if($saloon == "mokotow"){
        $url = get_home_url() . "/serwis-warszawa-mokotow" . $anchor;
     }
     else if($saloon == "krakow"){
        $url = get_home_url() . "/serwis-krakow" . $anchor;
     }
     else if($saloon == "katowice"){
        $url = get_home_url() . "/serwis-katowice" . $anchor;
     }

    //CONFIRMATION PAGE
     ob_start(); ?>
        <div class="ms-confirmation">
            <div class="ms-confirmation__icon">
                <img src="<?php echo WW_URL . 'icons/calendar-coloured.svg';  ?>" alt="">
            </div>
            <h3 class="ms-confirmation__heading">Twoja wizyta została potwierdzona!</h3>
            <p class="ms-confirmation__desc">Dziękujemy za informację. Zapraszamy do jednego z naszych salonów w dogodnym przez Ciebie dniu i godzinie - godziny otwarcia naszych salonów znajdziesz <a href="<?php echo $url; ?>">tutaj</a> .
            </br>
            </br>
           <span> Nie wiesz jak do nas trafić? <a href="#">Nawiguj do salonu</a></span></p>
            <button onclick="backToHomePage();" class="ms-form__button ms-form__button--next">Strona główna</button>
        </div>
    <?php $confirmationPage = ob_get_clean(); 

    $ok = ["data" => $confirmationPage, "status" => "ok"];
    $json = json_encode($ok);
    echo $json;

    function sendEmailToFlix($saloon) {
        $to = "baciey@gmail.com";
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $subject = "Potwierdzenie wizyty w salonie";
        $message = "Zarejestrowano zgłoszenie wizyty w salonie: " . $saloon;
    
        $emailresult = wp_mail($to, $subject, $message, $headers);
    }

    sendEmailToFlix($saloon);
 }






if ($deviceData["step1"]["id"] == 3) createShipment($authData, $formData, $deviceData);
else if ($deviceData["step1"]["id"] == 2) visitUsConfirmationPage($formData);






?>