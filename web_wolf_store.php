<?php

/*
Plugin Name: Web Wolf store
Description: A place to store custom files
Author: MB WEB WOLF
Version: 1.4
*/

define('WW_DIR', plugin_dir_path(__FILE__));
define('WW_URL', plugin_dir_url(__FILE__));

function web_wolf_store() {

    $classes = get_body_class();
    if (in_array('page-id-638',$classes)) {

        wp_enqueue_style( 'ww-custom-css', WW_URL . "custom-css.css",false,'1.1','all');
        wp_enqueue_script( 'ww-custom-js', WW_URL . "custom-js.js", array (  ), null, true);
        wp_localize_script( 'ww-custom-js', 'url', array( // function for passing needed data to custom script, in our case to script with ww-custom-js handler
           'ajax'    => admin_url( 'admin-ajax.php' ), // url for ajax requests
           'plugin_path' => WW_URL // plugin URL path
       ) );
       
    } 

         
 }


    add_action('wp_enqueue_scripts', 'web_wolf_store');

 add_action( 'wp_ajax_nopriv_save-ajax-request', 'saveRating' ); // wordpress ajax hook for nonlogged users
 add_action( 'wp_ajax_save-ajax-request', 'saveRating' ); // wordpress ajax hook for logged users
 // in our case save-ajax-request is an action from msFormData object sended from the frontend

 function saveRating() {
     // $_POST is our data sended by user after "Wyślij zgłoszenie" button click
     // you can do with this data whatever you want :)
     wp_send_json($_POST);

 }

?>