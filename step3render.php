<?php require_once('requiredAjaxCallCode.php'); ?>
<div class="ms-form__options-container ms-form__options-container--step-3">
    <?php function step3() {
        $data = json_decode(file_get_contents('php://input'), true);
        $id = $data["step2"]["id"];
       
        $args = array(
            'post_type' => 'model',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'device-types',
                    'field'    => 'term_id',
                    'terms'    => $id,
                ),
            ),
        );

        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="ms-form__option--step-3 ms-form__option" data-name="<?php echo the_title(); ?>" id="<?php echo the_ID(); ?>">
                <?php $icon = get_the_post_thumbnail_url();
                    if($icon) { ?>
                    <div class="ms-form__col-icon"><img src="<?php echo $icon; ?>" alt=""></div>
                <?php } ?>
                    <div class="ms-form__col-title"><?php echo the_title(); ?></div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); 
    }
    step3(); ?>
</div>

