<?php require_once('requiredAjaxCallCode.php'); ?>
<div class="ms-form__options-container ms-form__options-container--step-4">
    <?php function step4() {
        $data = json_decode(file_get_contents('php://input'), true);
        $id = $data["step3"]["id"];
        $post_id = 'post_' . $id;
        // $services = get_field('uslugi', $post_id);

        $args = [
            'posts_per_page'   => -1,
            'post_type' => 'usluga',
            
        ];

        $all_services = new \WP_Query( $args );

        while ( $all_services->have_posts() ) : $all_services->the_post();

            if( have_rows('service_repeater') ) :
                while( have_rows('service_repeater') ) :  the_row();

                    if( (int) $id === (int) get_sub_field('model_name') ) :

                        $icon = get_the_post_thumbnail_url();

                        ?>

                            <div class="ms-form__option--step-4 ms-form__option" data-name="<?php echo get_the_title() ?>" id="<?php echo get_the_ID() ?>" data-price_substitute="<?php echo get_sub_field('replacemet_part_price') ?>"  data-price_original="<?php echo get_sub_field('original_part_price') ?>" >
                                <?php 
                                    if($icon) { 
                                        ?>
                                            <div class="ms-form__col-icon"><img src="<?php echo $icon; ?>" alt=""></div>
                                        <?php 
                                    } 
                                ?>

                                <div class="ms-form__col-title"><?php echo get_the_title() ?></div>
                            </div>

                        <?php

                    endif;

                endwhile;
            endif;

        endwhile;

        
       
?>
            
    <?php }
    step4(); ?>
</div>